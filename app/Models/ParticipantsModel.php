<?php
namespace App\Models;

use CodeIgniter\Model;
class ParticipantsModel extends Model{
    protected $table = 'participants';
    protected $primaryKey = 'id';
    protected $allowedFields = ['name', 'profession', 'dob', 'locality', 'address', 'guest'];
}