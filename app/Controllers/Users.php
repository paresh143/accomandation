<?php namespace App\Controllers;
use App\Models\UserModel;

class Users extends BaseController
{
	public function index()
	{
		$data = [];
		if (session()->get('isLoggedIn')) {
			return redirect()->to(base_url() . '/participants');
		}

		helper(['form']);
		
		if ($this->request->getMethod() == 'post') {
			$rules = [
				'name' => 'required|min_length[3]|max_length[50]',
				'password' => 'required|min_length[5]|max_length[255]|validateUser[name,password]',
			];

			$errors = [
				'password' => [
					'validateUser' => 'Name or Password don\'t match'
				]
			];
			
			if (! $this->validate($rules, $errors)) {
				$data['validation'] = $this->validator;
			}else{
				$model = new UserModel();
				$user = $model->where('name', $this->request->getVar('name'))->first();
				$this->setUserSession($user);
				return redirect()->to(base_url() . '/participants');

			}
		}

		echo view('template/header', $data);
		echo view('login');
		echo view('template/footer');
	}

	private function setUserSession($user)
	{
		$data = [
			'id' => $user['id'],
			'name' => $user['name'],
			'isLoggedIn' => true,
		];
		session()->set($data);
		return true;
	}

	public function register()
	{
		$data = [];
		helper(['form']);

		if ($this->request->getMethod() == 'post') {
			//let's do the validation here
			$rules = [
				'name' => 'required|min_length[3]|max_length[50]|is_unique[users.name]',
				'password' => 'required|min_length[5]|max_length[255]',
				'password_confirm' => 'matches[password]',
			];

			if (! $this->validate($rules)) {
				$data['validation'] = $this->validator;
			}else{
				$model = new UserModel();

				$newData = [
					'name' => $this->request->getVar('name'),
					'password' => $this->request->getVar('password'),
				];

				$model->save($newData);
				$session = session();
				$session->setFlashdata('success', 'Successful Registration');
				return redirect()->to(base_url() . '/login');
			}
		}
		echo view('template/header', $data);
		echo view('register', $data);
		echo view('template/footer');
	}

	public function logout()
	{
		session()->destroy();
		return redirect()->to(base_url() . '/login');
	}
}