<?php namespace App\Controllers;

use CodeIgniter\Controller;
class Home extends Controller
{
	public function index()
	{
		echo view('template/header');
		echo view('home');
		echo view('template/footer');
	}

	//--------------------------------------------------------------------

}
