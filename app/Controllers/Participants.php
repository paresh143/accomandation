<?php namespace App\Controllers;

use CodeIgniter\RESTful\ResourceController;
use App\Models\ParticipantsModel;

class Participants extends ResourceController
{
	// get list of participants
	public function index($page=1)
	{
		if (!session()->get('isLoggedIn')) {
			return redirect()->to(base_url().'/login');
		}
		$name = '';
		$locality = '';
		$whereArr = [];
		
		helper(['form']);

		if ($this->request->getVar('name')) {
			$name = $this->request->getVar('name');
			$whereArr['name'] = $name;
		}

		if ($this->request->getVar('locality')) {
			$locality = $this->request->getVar('locality');
			$whereArr['locality'] = $locality;
		}

		$model = new ParticipantsModel();
		$data = [
        	'name' => $name,
			'locality' => $locality
		];
		if (empty($whereArr)) {
			$data['participants_list'] = $model->orderBy('id', 'desc')->paginate(10);
		} else {
			$data['participants_list'] = $model->where($whereArr)->orderBy('id', 'desc')->paginate(10);
		}
		$data['pager'] = $model->pager;

		echo view('template/header');
		echo view('participants_list', $data);
		echo view('template/footer');		
	}

	public function show($id = null) {
		helper(['form']);
		
		$data = [];

		if (!is_null($id) && !empty($id)) {
			if (!session()->get('isLoggedIn')) {
				return redirect()->to(base_url().'/login');
			}
			$model = new ParticipantsModel();
			$data['user'] = $model->where('id', $id)->first();
		}

		if (empty($data['user'])) {
			$data['method'] = 'POST';
			$data['action_url'] = base_url() . '/participants';

			$data['user'] = array(
				'id' => 0,
				'name' => '',
				'dob' => '',
				'profession' => 'employed',
				'locality' => '',
				'address' => '',
				'guest' => '0'
			);
		} else {
			$data['method'] = 'PUT';
			$data['action_url'] = base_url() . '/participants';
		}

		echo view('template/header');
		echo view('participants_form', $data);
		echo view('template/footer');
	}

	public function create() {
		helper(['form']);
		$data = [];
		$user = [
			'name' => @$this->request->getVar('name'),
			'profession' => @$this->request->getVar('profession'),
			'dob' => @$this->request->getVar('dob'),
			'locality' => @$this->request->getVar('locality'),
			'address' => @$this->request->getVar('address'),
			'guest' => @$this->request->getVar('guest')
		];
		
		if ($this->request->getVar('id')) {
			$user['id'] = $this->request->getVar('id');
			
			if (!session()->get('isLoggedIn')) {
				return redirect()->to(base_url().'/login');
			}
		}
		$data['user'] = $user;

		if ($this->request->getMethod() == 'post' || $this->request->getMethod() == 'put' ) {
			//let's do the validation here
			$rules = [
				'name' => 'required|min_length[3]|max_length[50]|alpha_space',
				'profession' => 'required|in_list[employed,student]',
				'dob' => 'required|valid_date[Y-m-d]',
				'locality' => "required|min_length[5]|max_length[80]|regex_match[/^[a-zA-Z0-9\s,'-]*$/]",
				'address' => "required|min_length[5]|max_length[255]|regex_match[/^[a-zA-Z0-9\s,'-]*$/]",
				'guest' => 'required|in_list[0,1,2]'
			];

			if (! $this->validate($rules)) {
				$data['validation'] = $this->validator;
			}else{
				$model = new ParticipantsModel();
				$model->save($user);
				$session = session();
				$session->setFlashdata('success', 'Participants saved Successfully');
				if (session()->get('isLoggedIn')) {
					return redirect()->to(base_url() . '/participants');
				} else {
					return redirect()->to(base_url().'/');
				}
			}
			$data['action_url'] = base_url() . '/participants';
			$data['method'] = 'post';
			echo view('template/header');
			echo view('participants_form', $data);
			echo view('template/footer');
		}
	}

}