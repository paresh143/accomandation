<div class="container">
    <div class="row">
        <div class="col-12 col-sm-8 offset-sm-2 col-md-6 offset-md-3 mt-5 pt-3 pb-3 bg-white from-wrapper">
            <div class="container">
                <?php if (session()->get('success')): ?>
                    <div class="alert alert-success" role="alert">
                        <?= session()->get('success') ?>
                    </div>
                <?php endif; ?>
                <h4>Hi welcome to advance accomandation booking system.<h4>
                <p>
                    <a href="<?= base_url() ?>/participants/show" >click here</a> to register your detail for accomandation
                </p>
            </div>
        </div>
    </div>
</div>