<div class="container">
    <div class="row">
        <div class="col-12 bg-white from-wrapper">
            <div class="container">
                <br/>
                <form class="" action="<?= base_url() ?>/participants" method="get">
                    <div class="row">
                        <div class="col-12 col-sm-4">
                            <div class="form-group">
                                <input type="text" class="form-control" placeholder="Name" name="name" id="name" value="<?= set_value('name', $name) ?>">
                            </div>
                        </div>
                        <div class="col-12 col-sm-4">
                            <div class="form-group">
                                <input type="text" class="form-control" placeholder="Locality" name="locality" id="locality" value="<?= set_value('locality', $locality) ?>">
                            </div>
                        </div>
                        <div class="col-12 col-sm-4">
                            <div class="form-group">
                                <button type="submit" class="btn btn-primary">Search</button>
                            </div>
                        </div>      
                    </div>
                </form>
                <div class="row">
                    <div class="col-12 col-sm-6">
                        <h3>All Registerd Participants List</h3>
                    </div>
                    <div class="col-12 col-sm-6 text-right">
                        <a href="<?= base_url() ?>/participants/show">New Participants</a>
                    </div>
                </div>                
                <div class="col-12">
                    <div class="row">
                        <table class="table table-bordered">
                            <thead>
                                <tr>
                                    <th>Id</th>
                                    <th>Name</th>
                                    <th>Birth Date</th>
                                    <th>Profession</th>
                                    <th>Locality</th>
                                    <th>Address</th>
                                    <th>guest</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php if ($participants_list): ?>
                                    <?php foreach ($participants_list as $participant): ?>
                                        <tr>
                                            <td title="#<?= $participant['id'] ?>">#<?php echo $participant['id']; ?></td>
                                            <td title="<?= $participant['name'] ?>"><?= $participant['name'] ?></td>
                                            <td title="<?= $participant['dob'] ?>"><?= $participant['dob'] ?></td>
                                            <td title="<?= $participant['profession'] ?>"><?= $participant['profession'] ?></td>
                                            <td title="<?= $participant['locality'] ?>"><?= $participant['locality'] ?></td>
                                            <td title="<?= $participant['address'] ?>"><?= $participant['address'] ?></td>
                                            <td title="<?= $participant['guest'] ?>"><?= $participant['guest'] ?></td>
                                            <td><a href="<?= base_url() ?>/participants/show/<?php echo $participant['id']; ?>">edit</a></td>
                                        </tr>
                                    <?php endforeach; ?>
                                <?php endif; ?>
                            </tbody>
                        </table>
                        <!-- Pagination -->
                        <div class="d-flex justify-content-end">
                            <?php if ($pager) :?>
                                <?php $pager->setPath('ci4/public/participants'); ?>
                                <?= $pager->only(['search', 'order'])->links(); ?>
                            <?php endif ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>