<!DOCTYPE html>
<html lang="en" dir="ltr">
    <head>
        <meta charset="utf-8">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
        <link rel="stylesheet" href="<?= base_url() ?>/assests/css/style.css" >
        <title></title>
    </head>
    <body>
        <?php $uri = service('uri'); ?>
        <nav class="navbar navbar-expand-lg navbar-dark bg-dark">
            <div class="container">
                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <?php if (session()->get('isLoggedIn')): ?>
                        <ul class="navbar-nav mr-auto">
                            <li class="nav-item <?= ($uri->getSegment(1) == '' ? 'active' : null) ?>">
                                <a class="nav-link" href="<?= base_url() ?>/">Home</a>
                            </li>
                            <li class="nav-item <?= ($uri->getSegment(1) == 'participants' ? 'active' : null) ?>">
                                <a class="nav-link"  href="<?= base_url() ?>/participants">Participants</a>
                            </li>
                        </ul>
                        <ul class="navbar-nav my-2 my-lg-0">
                            <li class="nav-item">
                                <a class="nav-link" href="<?= base_url() ?>/logout">Logout</a>
                            </li>
                        </ul>
                    <?php else: ?>
                        <ul class="navbar-nav mr-auto">
                            <li class="nav-item <?= ($uri->getSegment(1) == '' ? 'active' : null) ?>">
                                <a class="nav-link" href="<?= base_url() ?>/">Home</a>
                            </li>
                            <li class="nav-item <?= ($uri->getSegment(1) == 'login' ? 'active' : null) ?>">
                                <a class="nav-link" href="<?= base_url() ?>/login">Login</a>
                            </li>
                        </ul>
                    <?php endif; ?>
                </div>
            </div>
        </nav>