<div class="container">
    <div class="row">
        <div class="col-12 col-sm-8 offset-sm-2 col-md-6 offset-md-3 mt-5 pt-3 pb-3 bg-white from-wrapper">
            <div class="container">
                <h3>Register Participants detail</h3>
                <hr>
                <form name="accomandation_form" action="<?= $action_url ?>" method="POST">
                    <input type="hidden" name="_method" value="<?= $method ?>" />
                    <input type="hidden" name="id" id="id" value="<?= set_value('id', $user['id']) ?>">
                    <div class="row">
                        <div class="col-12">
                            <div class="form-group">
                                <label for="firstname">Name:</label>
                                <input type="text" class="form-control" name="name" id="name" value="<?= set_value('name', $user['name']) ?>">
                            </div>
                        </div>
                        <div class="col-12">
                            <div class="form-group">
                                <label for="dob">Date of Birth:</label>
                                <input type="date" class="form-control" name="dob" id="dob" value="<?= set_value('dob', $user['dob']) ?>">
                            </div>
                        </div>
                        <div class="col-12">
                            <div class="form-group">
                                <label for="profession">Profession:</label>
                                <select id="profession" name="profession" class="form-control" value="<?= set_value('profession', $user['profession']) ?>">
                                    <option value='employed'>Employed</option>
                                    <option value='student'>Student</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-12">
                            <div class="form-group">
                                <label for="locality">Locality:</label>
                                <input type="text" class="form-control" name="locality" id="locality" value="<?= set_value('locality', $user['locality']) ?>">
                            </div>
                        </div>
                        <div class="col-12">
                            <div class="form-group">
                                <label for="locality">Address:</label>
                                <input type="text" class="form-control" name="address" id="address" value="<?= set_value('address', $user['address']) ?>">
                            </div>
                        </div>
                        <div class="col-12">
                            <div class="form-group">
                                <label for="guest">Number of guest: </label>
                                <select id="guest" name="guest" class="form-control" value="<?= set_value('guest', $user['guest']) ?>">
                                    <option value='0'>0</option>
                                    <option value='1'>1</option>
                                    <option value='2'>2</option>
                                </select>
                            </div>
                        </div>
                        <?php if (isset($validation)): ?>
                            <div class="col-12">
                                <div class="alert alert-danger" role="alert">
                                    <?= $validation->listErrors() ?>
                                </div>
                            </div>
                        <?php endif; ?>
                    </div>
                    <div class="row">
                        <div class="col-12 col-sm-4">
                            <button type="submit" class="btn btn-primary">Submit</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>