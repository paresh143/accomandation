Tables to be Created

CREATE TABLE `participants` (
 `id` int NOT NULL AUTO_INCREMENT COMMENT 'PK AI',
 `name` varchar(255) NOT NULL COMMENT 'participants name',
 `profession` enum('employed','student') CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT 'employed',
 `dob` date NOT NULL COMMENT 'participants date of birth',
 `locality` varchar(255) NOT NULL COMMENT 'participants locality',
 `address` varchar(255) NOT NULL COMMENT 'participants address',
 `guest` tinyint NOT NULL DEFAULT '0',
 `created` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'created date',
 `updated` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT 'updated date',
 PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COMMENT='stores detail of each participants and guest'

CREATE TABLE `users` (
 `id` int NOT NULL AUTO_INCREMENT,
 `name` varchar(255) NOT NULL,
 `password` varchar(255) NOT NULL,
 `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
 `updated_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
 PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8


configuration variable in .env file

app.baseURL = 'http://localhost/ci4/public/'

database.default.hostname = localhost
database.default.database = eduvanz
database.default.username = root
database.default.password = root
database.default.DBDriver = MySQLi
